---
section: issue
title: DDOS Attack
date: 2021-04-09T11:55:39.314Z
resolved: false
draft: false
informational: false
resolvedWhen: ""
affected:
  - Indonesia Roleplay
  - Javanese Roleplay
  - Website
  - Forum
  - Radio
  - API
severity: down
---
*Investigating* - We are investigating a potential issue that might affect the uptime of six our of services. We are sorry for any inconvenience this may cause you. This incident post will be updated once we have more information.